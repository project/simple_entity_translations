Why do you need this module?
This module aims to make content translation easier. It adds two additional forms for content translation.
One form is to translate multiple entities at once. Second is to translate one entity to several languages at once.
Note: these forms do NOT display untranslatable fields!
You can select what content type, vocabulary (content entity bundle) should have such forms.

Installation:
 - Download or install it via Composer.
 - Enable the module.
 - Add permission 'Use simplified translation pages' (access simple entity translations).
 - Go to content type, vocabulary (content entity bundle) edit page and enable 'Use simplified translation form'.
 - Aftewards you will see additional tab (and operation) for such entities (single entity form) and for bundles as well (multiple entities form).
